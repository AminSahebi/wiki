## Scope and disclaimer

This page explains how to import Questions and Answers from Atlassian's Questions for Confluence to Smart Questions and Answers. You have to be admin to perform those actions.

:bangbang: The import functionality has been tested with a limited scope. We can't guarantee that all questions and their answers are transferred correctly. Please try the import into a test space first before going live.


## Know limitations of the import

The following known limitations exist:

* Votes are not imported.
* Images are imported but refer to the original image - once you delete the original Questions for Confluence question the image might be gone.
* Access limitations are neither evaluated nor maintained.
* Watches are not imported.

## The import process in detail

The following steps have to be performed for the import:

1. Create the target Smart Questions and Answers topic page (the page holding the overview macro). Keep in mind to import to a test space first.
2. Note the page Id of this page.
3. Evaluate the REST query you will use to collect all relevant questions from Questions fro Confluence. Please refer to https://docs.atlassian.com/confluence-questions/rest/resource_QuestionResource.html for details.
4. Ensure that the Confluence HTML macro is at least actives temporary.
5. Create a temporary page and include an HTML macro. The HTML macro has to hold the content stated below this list.
6. Save the page.
7. Hit the "Import questions into Smart Questions and Answers" button.
8. Enter the page Id you noted in step 2.
9. Enter the url you evaluated in step 3. Start with /rest/questions/1.0/...
10. Observe the result - the script will add lines below the button. In case nothing is shown check the console or network log. The import is completed with the line "Done" being printed.

:bulb: Alternative to enabling the HTML macro you can also create a user macro.

:question: In case you need help just raise a [support ticket](https://purde-software.atlassian.net/servicedesk/customer/portal/2).


```
#!HTML

<div>
	<button class="aui-button" type="button" onclick="javascript:queryQuestions();">Import questions into Smart Questions and Answers</button>
</div>
<p></p>
<div id="import-results"></div>

<script>
	var topicPageId = 0;

	function queryQuestions() {
		topicPageId = prompt("Please enter the target topic page Id", 0);
		var questionQueryUrl = prompt("Please enter the Confluence Questions query url", "/rest/questions/1.0/question?limit=10");
		if((topicPageId != null) && (questionQueryUrl != null))
		{
			AJS.$.ajax({
				url: AJS.contextPath() + questionQueryUrl,
				type: "GET",
				dataType: "json",
				success: function(result) {
					getSingleQuestion(result, 0);
				}
			});
		}
	}

	function getSingleQuestion(questions, questionCounter) {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/questions/1.0/question/" + questions[questionCounter].id,
			type: "GET",
			dataType: "json",
			success: function(result) {
				importSingleQuestion(result, questions, questionCounter);
			}
		});
	}

	function importSingleQuestion(question, questions, questionCounter) {

		var container = {
			topicPageId: 0,
			title: "",
			name: "",
			dateLong: 0,
			body: "",
			answers : [],
			comments: [],
			labels : []	
		};

		container.topicPageId = topicPageId;
		container.title = question.title;
		container.dateLong = question.dateAsked;
		container.name = question.author.name;
		container.body = question.body.content;
		
		for(var n = 0; n < question.topics.length; n++) {
			var label = {};
			label.name = question.topics[n].name;
			container.labels.push(label);
		}

		for(var n = 0; n < question.comments.length; n++) {
			var comment = {};
			comment.name = question.comments[n].author.name;
			comment.dateLong = question.comments[n].dateCommented;
			comment.body = question.comments[n].body.content;
			container.comments.push(comment);
		}

		for(var n = 0; n < question.answers.length; n++) {
			var answer= {};
			answer.name = question.answers[n].author.name;
			answer.dateLong = question.answers[n].dateAnswered;
			answer.body = question.answers[n].body.content;
			answer.approved = question.answers[n].accepted;
			answer.comments = [];
			for(var m = 0; m < question.answers[n].comments.length; m++) {
				var comment = {};
				comment.name = question.answers[n].comments[m].author.name;
				comment.dateLong = question.answers[n].comments[m].dateCommented;
				comment.body = question.answers[n].comments[m].body.content;
				answer.comments.push(comment);
			}
			container.answers.push(answer);
		}

		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/simpleqa/1.0/addquestion",
			type: "PUT",
			dataType: "json",
			contentType: "text/plain",
			data: JSON.stringify(container),
			success: function(result) {
				AJS.$("#import-results").append("<p>" + question.title + " - "+ JSON.stringify(result) + "</p>");
				questionCounter++;
				if(questionCounter < questions.length) {
					getSingleQuestion(questions, questionCounter);
				}
				else {
					AJS.$("#import-results").append("<p>Done</p>");
				}
			}
		});
	}

</script>
```