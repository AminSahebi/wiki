# Smart Questions and Answers Confluence Server app


## :bulb: This documentation is for the Server and Data Center version. Cloud users take [this way](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/1929904139/Smart+Questions+and+Answers+for+Confluence+Cloud).

[TOC]

---

## Scope
This Wiki page is the basic user documentation for the Smart Questions and Answers (formerly known as Simple Questions and Answers) Confluence app. It contains (hopefully) all necessary detail to use the appeffectively.

:bulb: Not all features are available in the free version (non-licensed version).

:bulb: As we only changed the "outer" branding from Simple Questions and Answers to Smart Questions and Answers in August 2018 you will still see text elements containing "simple-qa" in many places.

---


## What is it?

The Smart Questions and Answers plug-in brings flexible question and answer functionality to Confluence. Unlike the alternatives this app uses pages as questions and comments as answers. This approach allows you to use all features of Confluence (including all your plug-ins) to be used when raising questions or giving answers. Furthermore it allows you export questions and their answers, to define your own template for questions etc.




### Compatibility
* Versions 1.7.0 and below are compatible with Confluence version 5.8.x and above.
* Versions 1.8.0 up to 1.11.0 are compatible with Confluence version 5.10.x and above.
* Versions 1.13.0 and above are compatible with Confluence version 6.0.x and above.

Please refer to the Marketplace for more details.

---


## How to get it?
There are two ways of getting the plug-in:

* The latest versions (free and commercial) via the [Atlassian Marketplace](https://marketplace.atlassian.com/apps/1217690)
* The older free versions are [here](https://bitbucket.org/apurde/simple-qa-free/downloads)

---


## Privacy policy

Please refer to the our [privacy policy](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/243466241/Privacy+policy).

---

## Source code license

Please refer to [this page](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement).

---

## End user license agreement

Please refer to [this page](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/778731523/End+user+license+agreement+EULA).


---

## Configuration

### Basic configuration

Smart Questions and Answers has the following parameters which can be configured on the system- and/or space-level. 
:bulb: Settings on the space-level always overwrite the settings on the system-level.

* Page used for system- or space-wide questions and answers: if you configure a page here a link from the header bar with the title "Questions" will be shown which links to the specified page (see picture below).
* Allow nested Smart Questions and Answers topics: if checked users may create their own nested topics on pages as before. When not checked the Smart-QA Overview macro will not render if the page is not the system- or space-wide questions and answers page.
* Show top askers and respondents: when checked the statistics macro is allowed to show the top-10 askers and respondents. As this might be a workers council concern we decided to allow a disabling here.

![questions.png](https://bitbucket.org/repo/6455LEM/images/3823508068-questions.png)


### Anonymous access

Smart Questions and Answers supports anonymous questions and answers in case open the space and page to the public. Please note that anonymous users can't vote an can't approve an answer. Of course they also don't receive any notifications. Please refer to the Confluence documentation on how to setup public access. You may wan't to consider granting the following for your public space:

* All - view
* Pages - add (to ask questions)
* Comments - add (to give answers)

:warning: Due the Confluence's rights management an anonymous user can always change the questions of other other anonymous users  

---


## Basic usage


### Step 1: Understand the idea

![smart_qa_concept_for_wiki.png](https://bitbucket.org/repo/6455LEM/images/2959871526-smart_qa_concept_for_wiki.png)

Before you start using the plugin you should understand the idea behind it:

* The plugin uses Confluence pages as questions and the comments on each page as answers to the particular question. The plug-in ensures that they look like this
* All questions are first-level child-pages of the "topic"-page holding the "Smart QA Overview" macro.
* The questions you will see in the overview are only those questions which are a child of the "topic"-page. You may want to have multiple "topic"- or overview-pages in your Confluence instance - they will not interfere.

:bulb: You can defined system- or space-wide "topics" which are shown as link in the header bar. Please refer to the configuration section above.

---

### Step 2: Include the Smart QA Overview macro

Include the Smart QA Overview macro on any page with no child pages (why: child pages are questions). When done you will see a search field, a button to create a new question a row of filter tabs and most likely the information that there are no questions (yet).

![overview_ui.png](https://bitbucket.org/repo/6455LEM/images/253795463-overview_ui.png)

The Smart QA Overview macro has a couple of parameters you can configure. Please note that most of those parameters wont change the overview's behavior or look and feel itself but the way the plugin handles the questions and answers under this overview.

Basics - overview page

* Show label overview: shows all used labels starting with the most frequent one. You can filter for the label by clicking on it or watch/unwatch a label by clicking on the eye-symbol next to it.
* Community style overview: show the overview in a style comparable to the Atlassian Community.
* Initial filter and sort: per default Smart Questions and Answers shows all questions in the overview sorted by their last activity. In case you want to change this you can fill this field. To do so please leave the editor and bring the overview in the state you want to have (apply filter, sort and search as needed). Now copy the URL from your browser's address bar and paste it in this field. The field may look something like this: `https://example.com/display/QM/QM+questions?simpleqafilter=simple-qa-filter-all&simpleqasearch=&simpleqasort=0,1&simpleqapage=1`.
* Approval reminder: please enter the number of hours (e.g. 36) after which the author of a question is reminded to check whether his/her question has been answered. The hours count from the last activity on the question. In case you don't want a reminder leave the field empty. Please note that the reminder is only sent once in case there are no further activities after the last reminder. The check whether a reminder is due is triggered every time the question is viewed and whenever the reminder task is executed (default: once every day).

:bulb: To limit the user groups allowed to ask see next section below.

Basics - question pages

* Allow down-vote: if activated down-voting of an answer is also possible. This option was implemented as some workers councils might not like down-voting.
* Show similar questions: show similar questions on each question page. Remark: the plug-in identifies similar questions based on used labels and the title of the question compared to other questions (title, question and answers).
* Show links: when set each question page contains a set of links to show the instructions (see further below) to go back to main topic page and to ask another questions.
* Show instructions as pop-up: when set instructions are shown as pop-up on each question page explaining how to contribute.
* Hide create buttons: when set the Confluence own create buttons are hidden on question pages. You may want to activate this in case users are trying to create new questions via those buttons.
* Alternative position for answers: when set answers may use the space left to the similar questions. This feature is only effective when "Show similar questions" is selected and the width of the browser window is sufficient.
* Show comments below answers: show comments below answers rather than above them.

Rights

* Content manager: a list of users allowed to accept an answer additionally to the owner of the question and to convert questions to answers and back additionally to the respondent. This might be useful in case you need a kind of moderator or question-manager.
* User group allowed to ask: leave empty if all users are allowed to ask questions. Otherwise type all user groups allowed to ask as a comma-separated list.
* User group allowed to respond: leave empty if all users are allowed to respond to a question. Otherwise type all user groups allowed to respond as a comma-separated list.

Question template

* Template for new questions: you can specify any template or blueprint available in the current space as template for a new questions. The default is "Smart QA default" which is basically an empty page with some instructional text. Please refer to the section [Using templates](https://bitbucket.org/apurde/simple-qa/wiki/Home#markdown-header-using-templates) below to learn how to correctly set-up a template.
* Start question extract after: in case you are using templates it might be useful to specify a string after which the extract shown in the overview starts. Please refer to the section [Using templates](https://bitbucket.org/apurde/simple-qa/wiki/Home#markdown-header-using-templates) below to get more details.

Customized instructions

* Custom instructional message title: you can override the default instructional message title by providing your title here.
* Custom instructional message body: you can override the default instructional message body by providing your body in [Confluence storage format]https://confluence.atlassian.com/doc/confluence-storage-format-790796544.html) here.

Email templates

* You can specify a customized header and body of the "approval reminder", the "new question" and "answer accepted" emails in this section. You may use the placeholders as shown in the description below each field in the macro editor. In case you leave a field empty the default message is sent.

Others

* Exclude words: comma-separated list of words to exclude when searching for similar questions. Define such words might help to narrow down the search.

* Special features: you may enter comma-separated special features here. Special features do not belong to the core functionality. Currently the following special features exist:
    * hide-ask-button: hide the "Ask Question" button
    * hide-watch-menu: hide the watch-topic link
    * bypass-index (removed since version 2.18): bypass the Confluence index for collecting questions (much slower!)
    * bypass-edit-restriction: allow users to create questions which do not have a add/edit permission in the space (please open a support ticket to get the necessary details)
    * similarQuestionsBasedOnLabelsOnly: determine similar questions based on labels only
    * allow-private-questions: allow the asker to create a private question and make it public later. This might be helpful in case you want to answer your own question right away.

---

### Step 3: Ask your first question

Hit the button "Ask question" on the overview page. You will be prompted to enter the title of your new question. When you hit :ballot_box_with_check: a new page will be created with the title you just specified. You may now adopt the title and provide all the necessary details of your question on the Confluence page just created.

:bulb: When you create a new question as described above the body of the page will contain instructional text (and a hidden macro called "Smart QA Export View"). You are supposed to replace the instructional text with your question. In case you are using plugins like Source Editor please keep the hidden macro in place.

---

### Step 4: Providing answers or comments

Any user may now provide an answer or comment to your question.

**Providing an answer:** once you have entered an answer in the box at the bottom of the question page and clicked "Save as answer" you will see additional elements next to the answer. The arrow up is used to up-vote the answer, the arrow down to down-vote it (down-voting might be deactivated). The number in the middle shows the current vote-count of all users for that answer. In case you are the owner of the question you can also approve an answer by clicking on the checkmark - in case you are not the owner a green checkmark indicates that this answer was approved by the owner of the question.

**Providing a comment:** providing a comment works like providing an answer. You only hit the "Save as comment" button instead.

![pimped_comments.png](https://bitbucket.org/repo/6455LEM/images/3035116802-pimped_comments.png)

:bulb: You may also convert and answer to a comment or vice-versa later by clicking the link below the question/answer.

**What is this pop-up?** In case an instructional message pop-up has been defined by the author of the overview page you will see instructions in a pop-up. You may opt-out by clicking the corresponding button - your decision is only valid for this set of questions and answers.

:bulb: Your opt-out decision is stored in your browsers local storage. In case you delete the local storage the decision is reset.

---

### Step 5: Working with the overview

The user interface provided by the Smart QA Overview macro allows you to search for questions, filter and sort them.

:bulb: Keep in mind that you will only find questions belonging to this overview meaning you will only find questions which are first-level-child-pages below the page holding the Smart QA overview macro.

The search looks for text in questions and answers and will only display the matching questions. It behaves like other Confluence search fields.

You may want to filter the questions. Currently the following filters are implemented:

* All: show all questions
* Answered: show only questions which are answered
* Unanswered: show only questions which are not answered
* No accepted answer: show only questions which have no accepted answer (includes questions with no answers)
* Mine: only show questions raised by me
* Answered by me: only show questions to which I provided an answer or comment
* Watched by me: only show questions watched by me
* Liked by me: only show questions which I licked
* My favourites: only show questions which I marked as favourite 

Sorting can be achieved by clicking on the table headers. Per default Smart QA shows the question with the most recent activity at the top. 

The image below gives and example. It shows an active filter for "answered" questions and a descending sorting for views.

![overview_filter_sort.png](https://bitbucket.org/repo/6455LEM/images/1849963071-overview_filter_sort.png)

---

### Step 6: Working with labels

As Smart Questions and Answers follows a different concept (multiple topics rather than one central Q&A place) labels are not as essential as in other Q&A plug-ins. However Smart QA supports labels:

* You can specify labels for your question by specifying labels for the page holding the question.
* The Smart QA Overview will show all labels assigned to questions.
* By clicking on one of the labels in the overview your search is modified such that it only show questions holding that label.
* You can watch single labels in a topic (see next section).

---


## Getting notifications

### Watching

You can either watch an entire topic or labels within a topic. To watch the entire topic please use the "Watch topic" link shown next to the "Ask question" button. To watch a label please click on the eye-symbol next to the label in the used labels section (if configured). In the example below the user is not watching the topic but the labels "development" and "general_questions".

![Simple QA watch.png](https://bitbucket.org/repo/6455LEM/images/2095150704-Simple%20QA%20watch.png)

The watching functions of Smart QA are based in Confluence features - they are not entirely independent. This results in the following behavior:
* When you set a watch the plug-in scans all questions and adds the watch flag (eye-symbol) to all matching questions.
* You can manually un-watch a question and won't receive notifications for this question any more.
* When you ask a question or give an answer a watch is automatically added (if configured in Confluence).
* When you un-watch the entire topic all watch flags are removed form the questions (in case this is not prevented by a label-watch).
* When you un-watch a label all questions with this flag are un-watched in case they don't hold another label which you still watch.
* Whenever a new questions comes in which matches your watches you will be notified by the plug-in.
* Whenever a question is updated which you watch you will be notified by Confluence.
* Whenever an answer is given to a question you watch you will be notified by Confluence.

:bulb: You can see all your Q&A watches in your personal menu. There you can also remove watches.

### Approval notification

The person who provided an answer which was accepted by the owner of a question will get a notification.

### Approval reminder

You can [configure an approval reminder](https://bitbucket.org/apurde/simple-qa/wiki/Home#markdown-header-step-2-include-the-simple-qa-overview-macro) which reminds the owner of a question to check whether his question has been answered.

___


## Advanced usage

### Using templates

You can use any template or blueprint available in the current space for a new question. Per default Smart QA uses a very simple template for a new question (called "Smart QA default" in the selection list).

As Confluence does not export comments (or answers in our case) to e.g. PDF you have to include the Smart QA Export View macro at the end of your template in case you want answers to be exported. You have two possibilities to do so:

1. Include it via the macro browser.
2. (recommended) Use a source editor (e.g. [Confluence Source Editor](https://marketplace.atlassian.com/plugins/com.atlassian.confluence.plugins.editor.confluence-source-editor/server/overview)). We recommend this approach as you can hide the Smart QA Export View macro in the editor and avoid user confusion. To do so please insert the following lines at the end of your template using a source editor:

    
```
#!html    
<p class="simple-qa-hide">
   <ac:structured-macro ac:name="simple-qa-export-view" ac:schema-version="1"/>
</p>
```

:bulb: Not all blueprints respect the title you specified when asking a new question. You might need to type it again in the dialog.

When using templates you will most likely run into a different challenge. Per default the Smart QA Overview macro shows an excerpt of the question which starts at the beginning of the page. With the parameter "Start question extract after" you can specify a string after which the extract starts. In the example of a template below you should enter "Details" under "Start question extract after".

![extract_after.png](https://bitbucket.org/repo/6455LEM/images/366960543-extract_after.png =200x)

In case you need the boxes for similar questions and the links to appear at a different location you can create a user macro (no body) with the content below and in include at the desired location of your template.


```
#!html

## @noparams

<div class="simple-qa-custom-box-location"></div>
```


---


### Getting some statistics

![simple_qa_statistics_logo.png](https://bitbucket.org/repo/6455LEM/images/3146109726-simple_qa_statistics_logo.png)

The Smart QA statistics macro can be used to show some basic indicators of your questions and answers topic. You have to specify the page holding the Smart QA overview macro (your topic). You can decide whether you want to show the top askers and respondents in the statistics and whether you want to include the topic title in the statistics.

:bulb: Whether you can show the top askers and respondents is configurable on system- and space-level.

The following special features exist:

* first-level-questions-only: only use first level questions in the statistic (some users use cascaded questions topics and requested this feature)

---

### Define side content in the "Community style"

![simple_qa_side_content.png](https://bitbucket.org/repo/6455LEM/images/305887661-simple_qa_side_content.png)

In case you are using the "Community style" in the Smart QA Overview macro you can define one or more blocks of side content shown on the right side of the questions. Just include one Smart QA Side Content macro for one block on the same page as the Smart QA Overview macro and put the content in the body of the macro.

![sidecontent.png](https://bitbucket.org/repo/6455LEM/images/3259464547-sidecontent.png)

---

### Get a list of question topics

![simple_qa_topic_list.png](https://bitbucket.org/repo/6455LEM/images/2959034675-simple_qa_topic_list.png)

Use the Smart QA Topic List macro to get a list of topics.

---

### Get all questions in a space or the entire instance

![simple_qa_collection.png](https://bitbucket.org/repo/6455LEM/images/2217206015-simple_qa_collection.png)

Use the Smart QA Collection macro to get all questions of the current space or the entire Confluence instance. Please note that you can't raise questions from the Smart QA Collection macro.

:bulb: Show the collection in community style and add a Smart QA side Content macro with the Smart QA Topic List macro inside the side content to get nice overview of your all your questions.

---

### Prevent isolated pages from being rated as a question

Smart Questions and Answers interprets all pages below a topic page as question. You may exclude isolates pages from being rated as question by setting the label "smart-qa-no-question" on that page.

---

### REST interface

| REST call | type | description |
|--|--|--|
| /rest/simpleqa/1.0/{topicPageId}/forcereminder | GET | Force an approval reminder to be send now regardless of any timings. Replace {topicPageId} by the page ID which holds the Smart QA Overview macro. |
| /rest/simpleqa/1.0/{topicPageId}/getwatches | GET | Get all watches for that topic. Returns the watches in JSON format. Replace {topicPageId} by the page ID which holds the Smart QA Overview macro. |
| /rest/simpleqa/1.0/{topicPageId}/watchlabelfor?label={name of the label or entiretopic}&watch={true/false}&userName={name of the user} | GET | Add/remove a watch for a label or the entire topic (label=entiretopic) the user with the given user name. To add a watch watch has to be true, to remove a watch watch has to be set to false. Replace {topicPageId} by the page ID which holds the Smart QA Overview macro.  | 
| /rest/simpleqa/1.0/addquestion | PUT | Add a question including answers/comments to a defined topic. This call is typically used when importing questions from other tools like Confluence Questions. The data payload is of this PUT request is the text representation of the below JSON format. Beside "date" also "dateLong" is supported which takes the date as long value.  | 


```
#!JSON

{
	topicPageId: 20905989,
	title: "Title of the question",
	name: "fritz",
	date: "2019-01-01",
	body: "<p>Body of the question</p>",
	answers : [
		{
			name: "sophie",
			date: "2019-03-01",
			body: "<p>Answer 1</p>",
			approved: false,
			comments : [
				{
					name: "sophie",
					date: "2019-03-01",
					body: "<p>Comment to the answer</p>"
				}
			]
		},
		{
			name: "john",
			date: "2019-03-02",
			body: "<p>Answer 2</p>",
			approved: true,
			comments : []
		}
	],
	comments: [
		{
			name: "sophie",
			date: "2019-03-01",
			body: "<p>Comment to the question</p>"
		}
	],
	labels : [
		{
			name: "test_label"
		}
	]	
}
```

---

### Importing from Questions for Confluence

In case you want to import questions from Atlassian's Questions for Confluence please refer to [this page](https://bitbucket.org/apurde/simple-qa/wiki/Importing%20questions%20from%20Confluence%20Questions).

---

## Trouble-shooting

### Frequent cases

#### I'm listed as a user who can additionally accept an answer but I can't

Please ensure that the last change of the page holding the Smart QA Overview macro was made by the creator of the page or a Space- or Confluence-admin. This is necessary to prevent e.g. others from adding themselves.

#### Font sizes not correct in a PDF export of a page containing the Smart QA Overview

You have to to [customize your PDF output](https://confluence.atlassian.com/doc/customize-exports-to-pdf-190480945.html) as Confluence does not correctly define the necessary styles:

```
#!css
small { 
   font-size: smaller;
}
.aui-label {
   font-size: inherit;
}
```

#### No answers/comments shown in export

Please ensure that you have created the question using the "Ask question" button on the overview page. In case you created a question by other means the macro ensuring that the answers/comments are exported was not included in the page.

---

### Known limitation

The plug-in has the following known limitations:

* For temporary limitations please refer to the [issue tracker](https://bitbucket.org/apurde/simple-qa/issues).
* One overview page for one topic: Smart Questions and Answers only allows one overview page for one topic.
* Confluence mobile view is only partly supported (read only).
* Due to limitation in the Confluence API Smart Questions and Answers needs at least one user in the *confluence-administrators* group to extract the likes. In case you are not using the group you may alternative add a user to the group *smart-qa-liker-extractor-group*. This user should be allowed to view all questions in your instance.

---

### Getting support ###

Please use out [ticketing system](https://purde-software.atlassian.net/servicedesk/customer/portal/2) to create a support request regardless whether you found a bug or have a feature request. Existing bugs or feature requests are [here](https://bitbucket.org/apurde/simple-qa/issues).

:bulb: Please note that only users with a licensed version of Smart Questions and Answers are entitled for support.